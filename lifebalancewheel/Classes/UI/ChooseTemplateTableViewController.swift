//
//  ChooseTemplateTableViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class ChooseTemplateTableViewController: UITableViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = NSLocalizedString("ChooseTemplate.Title", comment:"")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ApplicationData.sharedInstance.templateArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "standartCell", for: indexPath)

        let template = ApplicationData.sharedInstance.templateArray[indexPath.row]
        
        cell.textLabel?.text = template.name
        // Configure the cell...

        return cell
    }
    
    func showWrongAlert ()
    {
        let alertController: UIAlertController = UIAlertController(title: "Information".localized , message:"AddSections.AddMin3SectionsFull".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "OK", comment:""), style: .cancel) { action -> Void in
            
        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }

 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        NSLog ("Did select row at index path")
        
        let template = ApplicationData.sharedInstance.templateArray[indexPath.row]

        if template.sections.count > 2
        {
            ApplicationData.sharedInstance.currentFilledTemplate = nil
            ApplicationData.sharedInstance.selectedTemplateIndex = indexPath.row

            self.performSegue(withIdentifier: "showTemplateFilling", sender: nil)
        }
        else
        {
            self.showWrongAlert()
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    
}
