//
//  AboutAppViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 10/5/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class AboutAppViewController: AdViewController {

    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet var buttons: Array<UIButton>!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "Main.AboutApp".localized;
        
        
        self.textView.text = self.textView.text.localized
        LocalizedManager.localizeButtonsArray(buttonsArray: buttons)
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(AboutAppViewController.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                               object: nil)

    }
    
    func handlePurchaseNotification(_ notification: Notification)
    {
        guard let productID = notification.object as? String else { return }
        
        for (_, product) in PurchaseManager.sharedInstance.products.enumerated()
        {
            guard product.productIdentifier == productID else
            {
                continue
            }
            
            self.showSuccessPurchaseAlert()
            
        }
    }

    func showSuccessPurchaseAlert ()
    {
        self.bannerView?.removeFromSuperview()
        self.bannerView = nil
        
        let alertController: UIAlertController = UIAlertController(title: "Information".localized , message:"PurchaseAlert.SuccessPurchase".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "OK".localized, comment:""), style: .cancel) { action -> Void in
            
        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func purchaseFullVersion(_ sender: AnyObject)
    {
        if PurchaseManager.sharedInstance.products.count > 0
        {
            let product = PurchaseManager.sharedInstance.products[0]
            IAProducts.store.buyProduct(product)
        }
    }

    @IBAction func restorePurchase(_ sender: AnyObject)
    {
        IAProducts.store.restorePurchases()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
