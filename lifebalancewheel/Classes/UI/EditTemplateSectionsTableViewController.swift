//
//  EditTemplateSectionsTableViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/29/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class EditTemplateSectionsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = "AddSections.Title".localized
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

   
    @IBAction func addSectionClick(_ sender: AnyObject)
    {
        let alertController: UIAlertController = UIAlertController(title:"Alert.CreateNewSection".localized , message:"Alert.EnterSectionName".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString("Cancel", comment:""), style: .cancel) { action -> Void in
        }
        
        alertController.addAction(cancelAction)
        
        //Create an optional action
        let nextAction: UIAlertAction = UIAlertAction(title:NSLocalizedString("Add", comment:""), style: .default) { action -> Void in
            
            let template = ApplicationData.sharedInstance.templateArray[ApplicationData.sharedInstance.selectedTemplateIndex]
            
            let text = ((alertController.textFields?.first)! as UITextField).text
//            template.name = text
            
            template.sections.append(text! as NSString)
           // ApplicationData.sharedInstance.templateArray.append(template)
            
            self.tableView.reloadData()
            
            ApplicationData.sharedInstance.saveTemplates()
        }
        
        alertController.addAction(nextAction)
        
        //Add text field
        alertController.addTextField { (textField) -> Void in
            //            textField.textColor = UIColor.green
        }
        //Present the AlertController
        present(alertController, animated: true, completion: nil)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let template = ApplicationData.sharedInstance.templateArray[ApplicationData.sharedInstance.selectedTemplateIndex]
        
        return template.sections.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let template = ApplicationData.sharedInstance.templateArray[ApplicationData.sharedInstance.selectedTemplateIndex]
        
        cell.textLabel?.text = template.sections[indexPath.row] as String
        
        
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "AddSections.AddMin3Sections".localized
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
