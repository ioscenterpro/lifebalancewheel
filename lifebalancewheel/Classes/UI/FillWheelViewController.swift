//
//  FillWheelViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class FillWheelViewController: FullPageAdViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
   
    @IBOutlet weak var drawingView: DrawingView!
    
    var isEditTemplate = false
    
    var currentFilledTemplate: FilledTemplate? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        if (ApplicationData.sharedInstance.currentFilledTemplate != nil)
        {
            currentFilledTemplate = ApplicationData.sharedInstance.currentFilledTemplate
            isEditTemplate = true
        }
        else
        {
            currentFilledTemplate = FilledTemplate ()
            ApplicationData.sharedInstance.currentFilledTemplate = currentFilledTemplate
            
            let template = ApplicationData.sharedInstance.templateArray[ApplicationData.sharedInstance.selectedTemplateIndex]
            
            currentFilledTemplate?.name = template.name
            currentFilledTemplate?.date = NSDate() as Date!
            currentFilledTemplate?.sections = []
            
            for sectionName in template.sections
            {
                let filledSection = FilledSection ()
                
                filledSection.name = sectionName as String!
                filledSection.value = 5
                
                if (filledSection.color == nil)
                {
                    filledSection.color = filledSection.getRandomColor()
                }
                
                currentFilledTemplate?.sections.append(filledSection)
            }
        }
        
        self.title = "FillTemplate.Title".localized
        self.saveButton.title = "Save".localized
        self.segmentControl.setTitle("FillTemplate.Filling".localized, forSegmentAt: 0)
        self.segmentControl.setTitle("FillTemplate.Preview".localized, forSegmentAt: 1)
        
        self.drawingView.currentFilledTemplate = currentFilledTemplate
        
        self.tableView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TemplateListTableViewController.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                               object: nil)
    }

    @IBAction func segmentClick(_ sender: AnyObject)
    {
        if (segmentControl.selectedSegmentIndex == 0)
        {
            self.tableView.isHidden = false
            self.drawingView.isHidden = true
        }
        else
        {
            self.tableView.isHidden = true
            self.drawingView.isHidden = false

            self.drawingView.setNeedsDisplay()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        NSLog ("Index %i", ApplicationData.sharedInstance.selectedTemplateIndex)
//        let template = ApplicationData.sharedInstance.templateArray[ApplicationData.sharedInstance.selectedTemplateIndex]

        return (ApplicationData.sharedInstance.currentFilledTemplate?.sections.count)!
        
//        return template.sections.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellForFilling", for: indexPath) as! FillWheelTableViewCell

        let section = ApplicationData.sharedInstance.currentFilledTemplate?.sections[indexPath.row]
        
        cell.valueSlider.tag = indexPath.row
        cell.valueSlider.tintColor =  section?.color
        cell.valueSlider.value = Float(section!.value)
  
        cell.valueLabel.text = String (format:"%.0f", cell.valueSlider.value)
        
        cell.nameLabel.text = "\(indexPath.row + 1)) " + (section?.name)!
        
        return cell
    }

    @IBAction func tableCellSliderDidSlide(_ sender: UISlider)
    {
        let section = ApplicationData.sharedInstance.currentFilledTemplate?.sections[sender.tag]
        let newValue =  round(sender.value)

        section?.value = Int(newValue)
    }
  

    func showSuccessAlert ()
    {
        let alertController: UIAlertController = UIAlertController(title: "Information".localized , message:"Alert.FillingTemplateWasAdded".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "OK", comment:""), style: .cancel) { action -> Void in
            
            _ = self.navigationController?.popToRootViewController(animated: true)

        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showSaveAlert ()
    {
        let alertController: UIAlertController = UIAlertController(title: "Information".localized , message:"PurchaseAlert.BuyFullVersionSave".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "PurchaseAlert.NoThanks".localized, comment:""), style: .default) { action -> Void in
            
        }
        
        let okAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "PurchaseAlert.PurchaseFull".localized, comment:""), style: .cancel) { action -> Void in
            
            if PurchaseManager.sharedInstance.products.count > 0
            {
                let product = PurchaseManager.sharedInstance.products[0]
                IAProducts.store.buyProduct(product)
                
            }
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
        
    @IBAction func saveButtonClick(_ sender: AnyObject)
    {
/*        if PurchaseManager.sharedInstance.products.count > 0
        {
            let product = PurchaseManager.sharedInstance.products[0]
            if IAProducts.store.isProductPurchased(product.productIdentifier) == false
            {
                self.showPurchaseAlert()
                return
            }
        }
*/
        
        if (isEditTemplate == false)
        {
            ApplicationData.sharedInstance.filledTemplateArray.append(ApplicationData.sharedInstance.currentFilledTemplate!)
        }
        ApplicationData.sharedInstance.currentFilledTemplate = nil
        
        ApplicationData.sharedInstance.saveFilledTemplates()
        
        self.showSuccessAlert()
    }

    func handlePurchaseNotification(_ notification: Notification) {
        guard let productID = notification.object as? String else { return }
        
        for (_, product) in PurchaseManager.sharedInstance.products.enumerated()
        {
            guard product.productIdentifier == productID else { continue }
            
        }
    }
    
    
    // MARK: - Table view data source
    
    func showPurchaseAlert ()
    {
        let alertController: UIAlertController = UIAlertController(title: "Information".localized , message:"PurchaseAlert.BuyFullVersionTemplate".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "PurchaseAlert.NoThanks".localized, comment:""), style: .default) { action -> Void in
            
        }
        
        let okAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "PurchaseAlert.PurchaseFull".localized, comment:""), style: .cancel) { action -> Void in
            
            if PurchaseManager.sharedInstance.products.count > 0
            {
                let product = PurchaseManager.sharedInstance.products[0]
                IAProducts.store.buyProduct(product)
                
            }
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
