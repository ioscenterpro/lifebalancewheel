//
//  TemplateListTableViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class TemplateListTableViewController: UITableViewController {

//    var loadingView: MBProgressHUD? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.title = "Main.Templates".localized
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        NotificationCenter.default.addObserver(self, selector: #selector(TemplateListTableViewController.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                               object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handlePurchaseNotification(_ notification: Notification) {
        guard let productID = notification.object as? String else { return }
        
        for (_, product) in PurchaseManager.sharedInstance.products.enumerated()
        {
            guard product.productIdentifier == productID else { continue }
            
        }

//        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
    

    
    func showLoadingView()
    {
//        loadingView = MBProgressHUD.showAdded(to: self.view, animated: true)
//        loadingView?.mode = MBProgressHUDMode.indeterminate
//        loadingView?.label.text = "Loading".localized

    }
    // MARK: - Table view data source

    func showPurchaseAlert ()
    {
        let alertController: UIAlertController = UIAlertController(title: "Information".localized , message:"PurchaseAlert.BuyFullVersionTemplate".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "PurchaseAlert.NoThanks".localized, comment:""), style: .default) { action -> Void in
            
        }
        
        let okAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "PurchaseAlert.PurchaseFull".localized, comment:""), style: .cancel) { action -> Void in
          
            if PurchaseManager.sharedInstance.products.count > 0
            {
                self.showLoadingView()
                let product = PurchaseManager.sharedInstance.products[0]
                IAProducts.store.buyProduct(product)
                
            }
        }

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }

    
    @IBAction func addButtonClick(_ sender: AnyObject)
    {
/*        if PurchaseManager.sharedInstance.products.count > 0
        {
            let product = PurchaseManager.sharedInstance.products[0]
            if IAProducts.store.isProductPurchased(product.productIdentifier) == false
            {
                self.showPurchaseAlert()
                return
            }
        }        
*/        
        let alertController: UIAlertController = UIAlertController(title: "Alert.CreateNewTemplate".localized , message:"Alert.EnterTemplateName".localized , preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title:NSLocalizedString( "Cancel", comment:""), style: .cancel) { action -> Void in
        }

        alertController.addAction(cancelAction)
        
        //Create an optional action
        let nextAction: UIAlertAction = UIAlertAction(title:NSLocalizedString("Add", comment:""), style: .default) { action -> Void in
            
            let template = Template ()
           
            let text = ((alertController.textFields?.first)! as UITextField).text
            template.name = text
            
            ApplicationData.sharedInstance.templateArray.append(template)
            
            self.tableView.reloadData()
            ApplicationData.sharedInstance.saveTemplates()
        }

        alertController.addAction(nextAction)
        
        //Add text field
        alertController.addTextField { (textField) -> Void in
//            textField.textColor = UIColor.green
        }
        //Present the AlertController
        present(alertController, animated: true, completion: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ApplicationData.sharedInstance.templateArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "standartCell", for: indexPath)
        
        let template = ApplicationData.sharedInstance.templateArray[indexPath.row]
        
        cell.textLabel?.text = template.name
        // Configure the cell...
        
        return cell
    }

    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        ApplicationData.sharedInstance.selectedTemplateIndex = self.tableView.indexPathForSelectedRow!.row

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
