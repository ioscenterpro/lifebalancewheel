//
//  FillWheelTableViewCell.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/23/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class FillWheelTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var valueSlider: UISlider!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func valueSliderDidChange(_ sender: AnyObject)
    {
        let newValue =  round(self.valueSlider.value)

        self.valueSlider.setValue(newValue, animated: false)

        
        self.valueLabel.text = String (format:"%.0f", newValue)
        
    }
}
