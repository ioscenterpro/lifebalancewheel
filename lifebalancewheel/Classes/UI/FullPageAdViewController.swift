//
//  FullPageAdViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 10/6/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FullPageAdViewController: UIViewController, GADInterstitialDelegate {

    var interstitial: GADInterstitial!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showAdBanner()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAdBanner ()
    {
        if IAProducts.store.isProductPurchased(IAProducts.fullVersionId) == true
        {
            return
        }
        
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-6338089446632606/9039229976")
        interstitial.delegate = self;
        let request = GADRequest()
//        request.testDevices = [kGADSimulatorID, "2d515cf19f965aa9fad61753be043a14"];
        interstitial.load(request)
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial!)
    {
        interstitial.present(fromRootViewController: self)
    }
}
