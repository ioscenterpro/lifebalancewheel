//
//  DrawingView.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/27/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class DrawingView: UIView
{

    var currentFilledTemplate: FilledTemplate? = nil

    let bordersDX = 20.0
    
    let angleShift = CGFloat(M_PI/2)
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect)
    {
        self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        
        drawRing()
        // Drawing code
        drawSections()
        
        
    }

    internal func drawRing()->()
    {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.center.x,y: self.center.y), radius: CGFloat(self.frame.size.width/2 - CGFloat(self.bordersDX)), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.darkGray.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer)
    }
    
    internal func fillSection (_ index: Int)
    {
        print (index)
        
        let section1 = currentFilledTemplate!.sections[index] as FilledSection
        
//        var section2: FilledSection? = nil
        
        var index2 = index + 1
        
       if (index == currentFilledTemplate!.sections.count-1)
        {
//            section2 = currentFilledTemplate!.sections[0] as FilledSection
            index2 = 0
        }
        else
        {
//            section2 = currentFilledTemplate!.sections[index2] as FilledSection
        }
 
 
        let aPath = UIBezierPath()
        
        let radius1 = CGFloat((section1.value)!)*(CGFloat(self.frame.size.width/2 - CGFloat(self.bordersDX)))/10

        let labelRadius = (CGFloat(self.frame.size.width/2 - CGFloat(self.bordersDX)))/5

        
//        let radius2 = CGFloat((section2?.value)!)*(CGFloat(self.frame.size.width/2 - CGFloat(self.bordersDX)))/10
        
        let angle1 = CGFloat(index)*CGFloat(2*M_PI)/CGFloat(currentFilledTemplate!.sections.count)
        let angle2 = CGFloat(index2)*CGFloat(2*M_PI)/CGFloat(currentFilledTemplate!.sections.count)

        let x1 = self.center.x + radius1 * cos(angle1)
        let y1 = self.center.y + radius1 * sin(angle1)

        let x2 = self.center.x + radius1 * cos(angle2)
        let y2 = self.center.y + radius1 * sin(angle2)

        
//        let labelAngle = CGFloat((index+index2)/2)*CGFloat(2*M_PI)/CGFloat(currentFilledTemplate!.sections.count)

        print ("Angle 1 \(angle1) angle2: \(angle2)")
        
        var labelAngle = (angle1 + angle2)/2
       
        if (angle2 == 0)
        {
//            labelAngle = (angle1 + CGFloat(currentFilledTemplate!.sections.count))/2
            labelAngle = (angle1 + CGFloat(2*M_PI))/2

        }
        
        let labelX = self.center.x + labelRadius * cos(labelAngle)
        let labelY = self.center.y + labelRadius * sin(labelAngle)

        
        aPath.move(to: CGPoint(x:self.center.x, y:self.center.y))
        
        aPath.addLine(to: CGPoint(x:x1, y:y1))
        
        aPath.addLine(to: CGPoint(x:x2, y:y2))
//        aPath.close()
        
        section1.color!.setFill()
        aPath.fill()

        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = aPath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.darkGray.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer)
        
        
        //Draw label
/*        let size:CGFloat = 28.0 // 35.0 chosen arbitrarily
        let countLabel = UILabel()
        countLabel.text = "51111111"
        countLabel.textColor = UIColor.black
//        countLabel.textAlignment = .center
        countLabel.font = UIFont.systemFont(ofSize: 14.0)
/*        countLabel.bounds = CGRect(x:0.0, y:0.0, width: size, height: size)
        countLabel.layer.cornerRadius = size / 2
        countLabel.layer.borderWidth = 1.0
        countLabel.layer.backgroundColor = UIColor.clear.cgColor
        countLabel.layer.borderColor = UIColor.green.cgColor
*/
        countLabel.center = CGPoint(x:labelX,y: labelY)

        self.layer.addSublayer(countLabel.layer)
*/
        
        let size:CGFloat = 14.0 // 35.0 chosen arbitrarily
        let countLabel = CATextLayer()
        countLabel.string = String(index + 1)
        countLabel.foregroundColor = UIColor.black.cgColor
        countLabel.fontSize = 14.0
        countLabel.isWrapped = true
        countLabel.contentsScale = UIScreen.main.scale
        countLabel.alignmentMode = kCAAlignmentCenter
        
 //       countLabel.textColor = UIColor.black
        //        countLabel.textAlignment = .center
//        countLabel.font = UIFont.systemFont(ofSize: 14.0)
        /*        countLabel.bounds = CGRect(x:0.0, y:0.0, width: size, height: size)
         countLabel.layer.cornerRadius = size / 2
         countLabel.layer.borderWidth = 1.0
         countLabel.layer.backgroundColor = UIColor.clear.cgColor
         countLabel.layer.borderColor = UIColor.green.cgColor
         */
        countLabel.frame = CGRect(x:0, y:0, width: size, height: size)
        
        countLabel.position = CGPoint (x: labelX, y: labelY)
        
        self.layer.addSublayer(countLabel)
//        self.fillSection (index)


    }
    
    
    internal func drawSections()->()
    {
        for index in 0...(currentFilledTemplate!.sections.count-1)
        {
            let aPath = UIBezierPath()
            
            let radius = CGFloat(self.frame.size.width/2 - CGFloat(self.bordersDX))
            
            let angle = CGFloat(index)*CGFloat(2*M_PI)/CGFloat(currentFilledTemplate!.sections.count)
            
            let x = self.center.x + radius * cos(angle)
            let y = self.center.y + radius * sin(angle)
            
            aPath.move(to: CGPoint(x:self.center.x, y:self.center.y))
            
            aPath.addLine(to: CGPoint(x:x, y:y))
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = aPath.cgPath
            
            //change the fill color
            shapeLayer.fillColor = UIColor.clear.cgColor
            //you can change the stroke color
            shapeLayer.strokeColor = UIColor.darkGray.cgColor
            //you can change the line width
            shapeLayer.lineWidth = 1.0
            
            self.layer.addSublayer(shapeLayer)
            
            self.fillSection (index)

        }
    }
}
