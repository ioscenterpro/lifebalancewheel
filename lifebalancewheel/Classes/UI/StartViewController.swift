//
//  StartViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit
import StoreKit


class StartViewController: AdViewController
{
    @IBOutlet var buttons: Array<UIButton>!
  
    func requestProducts()
    {
        let products: [SKProduct] = []
        
        IAProducts.store.requestProducts{success, products in
            if success
            {
                PurchaseManager.sharedInstance.products = products!
            }
        }
    }
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ApplicationData.sharedInstance.loadData()
        
        NSLog ("Test".localized)
        
        NSLog  (NSLocalizedString("Test", comment:""))
        
        self.title = NSLocalizedString("App.Name", comment:"")
        
        LocalizedManager.localizeButtonsArray(buttonsArray: self.buttons)
        
        self.requestProducts()
        
//        self.showAdBanner()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
