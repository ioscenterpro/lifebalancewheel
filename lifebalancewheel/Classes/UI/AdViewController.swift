//
//  AdViewController.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 10/6/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdViewController: UIViewController, GADBannerViewDelegate
{
    var bannerView: GADBannerView?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAdBanner ()
    {
        if IAProducts.store.isProductPurchased(IAProducts.fullVersionId) == true
        {
            return
        }
        
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView?.adUnitID = "ca-app-pub-6338089446632606/7841698378"
        bannerView?.rootViewController = self
        bannerView?.delegate = self
        
        
        let request = GADRequest()
//        request.testDevices = [kGADSimulatorID, "2d515cf19f965aa9fad61753be043a14"];
        bannerView?.load(request)
        
        self.view.addSubview(bannerView!)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!)
    {
        print ("Did receive ad")
        
        bannerView!.frame = CGRect(x:0,y: self.view.frame.size.height - bannerView!.frame.size.height,
                                   width:bannerView!.frame.size.width,height:bannerView!.frame.size.height);
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.bannerView?.removeFromSuperview()
        self.bannerView = nil
//        self.showAdBanner()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
