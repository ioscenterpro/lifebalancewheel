//
//  FilledSection.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/27/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class FilledSection: NSObject, NSCoding
{
    var name:String!
    var value:Int!
    
    var color: UIColor?

    func getRandomColor() -> UIColor
    {
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
   
    
    override init()
    {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        if let name = aDecoder.decodeObject(forKey: "name") as? String
        {
            self.name = name
        }
        
        if let value = aDecoder.decodeObject(forKey: "value") as? Int
        {
            self.value = value
        }

        if let color = aDecoder.decodeObject(forKey: "color") as? UIColor
        {
            self.color = color
        }

        
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.value, forKey: "value")
        aCoder.encode(self.color, forKey: "color")

    }

    
}
