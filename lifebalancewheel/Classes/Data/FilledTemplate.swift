//
//  FilledTemplate.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/27/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class FilledTemplate: NSObject, NSCoding
{
    var name:String!
    var date:Date!
        
    var sections: [FilledSection] = []
    
    override init()
    {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        if let name = aDecoder.decodeObject(forKey: "name") as? String
        {
            self.name = name
        }
        
        if let date = aDecoder.decodeObject(forKey: "date") as? Date
        {
            self.date = date
        }
        
        if let sections = aDecoder.decodeObject(forKey: "sections") as? [FilledSection]
        {
            self.sections = sections
        }
        
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.sections, forKey: "sections")
        
    }

}
