//
//  ApplicationData.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit




class ApplicationData: NSObject
{
    static let sharedInstance = ApplicationData()
 
    var templateArray: [Template]! = nil
    var filledTemplateArray: [FilledTemplate]! = nil


    var currentFilledTemplate: FilledTemplate? = nil

    
    var selectedTemplateIndex = -1
    
    func getDocumentsDirectory() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0] as String
        return documentsDirectory
    }
    
    func createDefaultTemplate()
    {
        let template = Template ()
        
        template.name =  NSLocalizedString("Templates.Standart", comment:"")
        
        template.sections.append( NSLocalizedString("Template.Standart.Education", comment:"") as NSString)
        template.sections.append( NSLocalizedString("Template.Standart.Relationship", comment:"") as NSString)
        template.sections.append( NSLocalizedString("Template.Standart.Religion", comment:"") as NSString)
        template.sections.append( NSLocalizedString("Template.Standart.Hobby", comment:"") as NSString)
        template.sections.append( NSLocalizedString("Template.Standart.Work", comment:"") as NSString)
        template.sections.append( NSLocalizedString("Template.Standart.Health", comment:"") as NSString)
        
        templateArray.append(template)
    }

    func loadData()
    {
        let documentsDirectory = self.getDocumentsDirectory() as String
        let templatePath = documentsDirectory + "templates"
        let filledTemplatePath = documentsDirectory + "filledTemplates"
        
        if let loadedTemplatesArray = NSKeyedUnarchiver.unarchiveObject(withFile: templatePath) as? [Template]
        {
            print ("Have loaded templates array")
            print(loadedTemplatesArray)
            self.templateArray = loadedTemplatesArray
        }
        else
        {
            templateArray = []
            self.createDefaultTemplate()
        }
        
        if let loadedFilledTemplatesArray = NSKeyedUnarchiver.unarchiveObject(withFile: filledTemplatePath) as? [FilledTemplate]
        {
            print ("Have filled template array")
            self.filledTemplateArray = loadedFilledTemplatesArray
        }
        else
        {
            filledTemplateArray = []
        }
        
    }

    func saveTemplates ()
    {
        print ("Save templates")

        let documentsDirectory = self.getDocumentsDirectory() as String
        let templatePath = documentsDirectory + "templates"

        NSKeyedArchiver.archiveRootObject(self.templateArray, toFile: templatePath)
    }
    
    func saveFilledTemplates ()
    {
        let documentsDirectory = self.getDocumentsDirectory() as String
        let filledTemplatePath = documentsDirectory + "filledTemplates"
        
        NSKeyedArchiver.archiveRootObject(self.filledTemplateArray, toFile: filledTemplatePath)
    }


    func saveData()
    {
    }
}
