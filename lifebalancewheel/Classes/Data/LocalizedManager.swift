//
//  LocalizedManager.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 10/4/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class LocalizedManager: NSObject
{

    static func localizeButton(button: UIButton)
    {
        button.setTitle( NSLocalizedString(button.title(for: .normal)!, comment:""), for: .normal)
    }

    static func localizeButtonsArray(buttonsArray: Array<UIButton>)
    {
        for button in buttonsArray
        {
            self.localizeButton(button: button)
        }
    }
    
}
