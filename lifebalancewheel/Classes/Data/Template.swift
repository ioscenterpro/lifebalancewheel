//
//  Template.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import UIKit

class Template: NSObject, NSCoding
{
    var name:String!
    
    var sections: [NSString] = []
    
    
    override init()
    {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        if let name = aDecoder.decodeObject(forKey: "name") as? String
        {
            self.name = name
        }
        
        if let sections = aDecoder.decodeObject(forKey: "sections") as? [NSString]
        {
            self.sections = sections
        }

    }
    
    func encode(with aCoder: NSCoder)
    {
        print ("Encode with coder")
        
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.sections, forKey: "sections")

    }
}
