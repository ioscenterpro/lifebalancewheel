//
//  NSString+Localized.swift
//  lifebalancewheel
//
//  Created by Artyom Savelyev on 9/22/16.
//  Copyright © 2016 Artyom Savelyev. All rights reserved.
//

import Foundation


extension String
{
    var localized: String
    {
        return NSLocalizedString(self, comment:"")
//        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    
    func localizedWithComment(comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
}
